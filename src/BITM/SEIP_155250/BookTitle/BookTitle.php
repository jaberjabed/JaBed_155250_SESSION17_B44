<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;


class BookTitle extends DB
{

    private $id;
    private $book_name;
    private $author_name;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('bookName',$postData)){
            $this->book_name = $postData['bookName'];
        }

        if(array_key_exists('authorName',$postData)){
            $this->author_name = $postData['authorName'];
        }

    }


    public function store(){

        $arrData = array($this->book_name,$this->author_name);

        $sql = "INSERT into book_title(book_name,author_name) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('create.php');


    }





    public function index(){

        echo "I am inside: ".__METHOD__."<br>";

    }




}